import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class IOCAppChallenge1 {

	public static void main(String[] args) {

		ApplicationContext context = new FileSystemXmlApplicationContext("beans-challenge.xml");
		
		City city = (City) context.getBean("cityBeanId");

		city.cityName();
		
		((AbstractApplicationContext) context).close();
	}
}
