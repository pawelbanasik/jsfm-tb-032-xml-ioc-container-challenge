import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class IOCAppChallenge2 {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("beans-challenge.xml");
		
		City city = (City) context.getBean("cityBeanId");
		
		city.cityName();
		
		((AbstractApplicationContext) context).close();	
	}
}
